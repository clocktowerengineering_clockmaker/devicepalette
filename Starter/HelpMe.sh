#!/bin/bash

echo "I am dialing out for help! If asked, please enter the password."
sudo tailscale up
echo -e "Hooray! Dialing out!!!!"
# Loop until a key is pressed or 1 hr has elapsed
while true; do
read -t 3600 -n 1 key  # Read a single character within a 5-second
if [ $? = 0 ]; then
echo -e "\n\n\n\nThanks for using HelpMe!!!!"
sudo tailscale down
exit  # Exit the script if a key is pressed
else
echo "Waiting for a keypress"
fi
done
